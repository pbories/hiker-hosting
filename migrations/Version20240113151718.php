<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240113151718 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Create hosting table';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE hosting_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE hosting (id INT NOT NULL, place_id INT NOT NULL, address_id INT NOT NULL, name VARCHAR(255) NOT NULL, email VARCHAR(255) DEFAULT NULL, min_check_in_type TIME(0) WITHOUT TIME ZONE DEFAULT NULL, max_check_in_time TIME(0) WITHOUT TIME ZONE DEFAULT NULL, max_check_out_time TIME(0) WITHOUT TIME ZONE DEFAULT NULL, has_kitchen BOOLEAN DEFAULT NULL, has_supper BOOLEAN DEFAULT NULL, has_break_fast BOOLEAN DEFAULT NULL, has_washing_machine BOOLEAN DEFAULT NULL, has_wi_fi BOOLEAN DEFAULT NULL, has_sheets BOOLEAN DEFAULT NULL, has_towels BOOLEAN DEFAULT NULL, has_swimming_pool BOOLEAN DEFAULT NULL, accepts_credit_card BOOLEAN DEFAULT NULL, type VARCHAR(255) NOT NULL, breakfast_price INT DEFAULT NULL, supper_price INT DEFAULT NULL, opening_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, closing_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, is_motor_friendly BOOLEAN DEFAULT NULL, is_reduced_mobility_friendly BOOLEAN DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E9168FDADA6A219 ON hosting (place_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E9168FDAF5B7AF75 ON hosting (address_id)');
        $this->addSql('ALTER TABLE hosting ADD CONSTRAINT FK_E9168FDADA6A219 FOREIGN KEY (place_id) REFERENCES place (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE hosting ADD CONSTRAINT FK_E9168FDAF5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE phone_number ADD hosting_id INT NOT NULL');
        $this->addSql('ALTER TABLE phone_number ADD CONSTRAINT FK_6B01BC5BAE9044EA FOREIGN KEY (hosting_id) REFERENCES hosting (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_6B01BC5BAE9044EA ON phone_number (hosting_id)');
        $this->addSql('ALTER TABLE room ADD hosting_id INT NOT NULL');
        $this->addSql('ALTER TABLE room ADD CONSTRAINT FK_729F519BAE9044EA FOREIGN KEY (hosting_id) REFERENCES hosting (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_729F519BAE9044EA ON room (hosting_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE phone_number DROP CONSTRAINT FK_6B01BC5BAE9044EA');
        $this->addSql('ALTER TABLE room DROP CONSTRAINT FK_729F519BAE9044EA');
        $this->addSql('DROP SEQUENCE hosting_id_seq CASCADE');
        $this->addSql('ALTER TABLE hosting DROP CONSTRAINT FK_E9168FDADA6A219');
        $this->addSql('ALTER TABLE hosting DROP CONSTRAINT FK_E9168FDAF5B7AF75');
        $this->addSql('DROP TABLE hosting');
        $this->addSql('DROP INDEX IDX_729F519BAE9044EA');
        $this->addSql('ALTER TABLE room DROP hosting_id');
        $this->addSql('DROP INDEX IDX_6B01BC5BAE9044EA');
        $this->addSql('ALTER TABLE phone_number DROP hosting_id');
    }
}
