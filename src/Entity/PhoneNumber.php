<?php

namespace App\Entity;

use App\Repository\PhoneNumberRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\UX\Turbo\Attribute\Broadcast;

#[ORM\Entity(repositoryClass: PhoneNumberRepository::class)]
#[Broadcast]
class PhoneNumber
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?bool $isMobile = null;

    #[ORM\Column(length: 255)]
    private ?string $fullNumber = null;

    #[ORM\ManyToOne(inversedBy: 'phoneNumbers')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Hosting $hosting = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function isIsMobile(): ?bool
    {
        return $this->isMobile;
    }

    public function setIsMobile(bool $isMobile): static
    {
        $this->isMobile = $isMobile;

        return $this;
    }

    public function getFullNumber(): ?string
    {
        return $this->fullNumber;
    }

    public function setFullNumber(string $fullNumber): static
    {
        $this->fullNumber = $fullNumber;

        return $this;
    }

    public function getHosting(): ?Hosting
    {
        return $this->hosting;
    }

    public function setHosting(?Hosting $hosting): static
    {
        $this->hosting = $hosting;

        return $this;
    }
}
