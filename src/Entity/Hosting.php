<?php

namespace App\Entity;

use App\Repository\HostingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\UX\Turbo\Attribute\Broadcast;

#[ORM\Entity(repositoryClass: HostingRepository::class)]
#[Broadcast]
class Hosting
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\ManyToOne(inversedBy: 'hostings')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Place $place = null;

    #[ORM\OneToMany(mappedBy: 'hosting', targetEntity: Room::class, orphanRemoval: true)]
    private Collection $rooms;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Address $address = null;

    #[ORM\OneToMany(mappedBy: 'hosting', targetEntity: PhoneNumber::class, orphanRemoval: true)]
    private Collection $phoneNumbers;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $email = null;

    #[ORM\Column(type: Types::TIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $minCheckInTime = null;

    #[ORM\Column(type: Types::TIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $maxCheckInTime = null;

    #[ORM\Column(type: Types::TIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $maxCheckOutTime = null;

    #[ORM\Column(nullable: true)]
    private ?bool $hasKitchen = null;

    #[ORM\Column(nullable: true)]
    private ?bool $hasSupper = null;

    #[ORM\Column(nullable: true)]
    private ?bool $hasBreakFast = null;

    #[ORM\Column(nullable: true)]
    private ?bool $hasWashingMachine = null;

    #[ORM\Column(nullable: true)]
    private ?bool $hasWiFi = null;

    #[ORM\Column(nullable: true)]
    private ?bool $hasSheets = null;

    #[ORM\Column(nullable: true)]
    private ?bool $hasTowels = null;

    #[ORM\Column(nullable: true)]
    private ?bool $hasSwimmingPool = null;

    #[ORM\Column(nullable: true)]
    private ?bool $acceptsCreditCard = null;

    #[ORM\Column(length: 255)]
    private ?string $type = null;

    #[ORM\Column(nullable: true)]
    private ?int $breakfastPrice = null;

    #[ORM\Column(nullable: true)]
    private ?int $supperPrice = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $openingDate = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $closingDate = null;

    #[ORM\Column(nullable: true)]
    private ?bool $isMotorFriendly = null;

    #[ORM\Column(nullable: true)]
    private ?bool $isReducedMobilityFriendly = null;

    public function __construct()
    {
        $this->rooms = new ArrayCollection();
        $this->phoneNumbers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getPlace(): ?Place
    {
        return $this->place;
    }

    public function setPlace(?Place $place): static
    {
        $this->place = $place;

        return $this;
    }

    /**
     * @return Collection<int, Room>
     */
    public function getRooms(): Collection
    {
        return $this->rooms;
    }

    public function addRoom(Room $room): static
    {
        if (!$this->rooms->contains($room)) {
            $this->rooms->add($room);
            $room->setHosting($this);
        }

        return $this;
    }

    public function removeRoom(Room $room): static
    {
        if ($this->rooms->removeElement($room)) {
            // set the owning side to null (unless already changed)
            if ($room->getHosting() === $this) {
                $room->setHosting(null);
            }
        }

        return $this;
    }

    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function setAddress(Address $address): static
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return Collection<int, PhoneNumber>
     */
    public function getPhoneNumbers(): Collection
    {
        return $this->phoneNumbers;
    }

    public function addPhoneNumber(PhoneNumber $phoneNumber): static
    {
        if (!$this->phoneNumbers->contains($phoneNumber)) {
            $this->phoneNumbers->add($phoneNumber);
            $phoneNumber->setHosting($this);
        }

        return $this;
    }

    public function removePhoneNumber(PhoneNumber $phoneNumber): static
    {
        if ($this->phoneNumbers->removeElement($phoneNumber)) {
            // set the owning side to null (unless already changed)
            if ($phoneNumber->getHosting() === $this) {
                $phoneNumber->setHosting(null);
            }
        }

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): static
    {
        $this->email = $email;

        return $this;
    }

    public function getMinCheckInTime(): ?\DateTimeInterface
    {
        return $this->minCheckInTime;
    }

    public function setMinCheckInTime(?\DateTimeInterface $minCheckInTime): static
    {
        $this->minCheckInTime = $minCheckInTime;

        return $this;
    }

    public function getMaxCheckInTime(): ?\DateTimeInterface
    {
        return $this->maxCheckInTime;
    }

    public function setMaxCheckInTime(?\DateTimeInterface $maxCheckInTime): static
    {
        $this->maxCheckInTime = $maxCheckInTime;

        return $this;
    }

    public function getMaxCheckOutTime(): ?\DateTimeInterface
    {
        return $this->maxCheckOutTime;
    }

    public function setMaxCheckOutTime(?\DateTimeInterface $maxCheckOutTime): static
    {
        $this->maxCheckOutTime = $maxCheckOutTime;

        return $this;
    }

    public function isHasKitchen(): ?bool
    {
        return $this->hasKitchen;
    }

    public function setHasKitchen(?bool $hasKitchen): static
    {
        $this->hasKitchen = $hasKitchen;

        return $this;
    }

    public function isHasSupper(): ?bool
    {
        return $this->hasSupper;
    }

    public function setHasSupper(?bool $hasSupper): static
    {
        $this->hasSupper = $hasSupper;

        return $this;
    }

    public function isHasBreakFast(): ?bool
    {
        return $this->hasBreakFast;
    }

    public function setHasBreakFast(?bool $hasBreakFast): static
    {
        $this->hasBreakFast = $hasBreakFast;

        return $this;
    }

    public function isHasWashingMachine(): ?bool
    {
        return $this->hasWashingMachine;
    }

    public function setHasWashingMachine(?bool $hasWashingMachine): static
    {
        $this->hasWashingMachine = $hasWashingMachine;

        return $this;
    }

    public function isHasWiFi(): ?bool
    {
        return $this->hasWiFi;
    }

    public function setHasWiFi(?bool $hasWiFi): static
    {
        $this->hasWiFi = $hasWiFi;

        return $this;
    }

    public function isHasSheets(): ?bool
    {
        return $this->hasSheets;
    }

    public function setHasSheets(?bool $hasSheets): static
    {
        $this->hasSheets = $hasSheets;

        return $this;
    }

    public function isHasTowels(): ?bool
    {
        return $this->hasTowels;
    }

    public function setHasTowels(?bool $hasTowels): static
    {
        $this->hasTowels = $hasTowels;

        return $this;
    }

    public function isHasSwimmingPool(): ?bool
    {
        return $this->hasSwimmingPool;
    }

    public function setHasSwimmingPool(?bool $hasSwimmingPool): static
    {
        $this->hasSwimmingPool = $hasSwimmingPool;

        return $this;
    }

    public function isAcceptsCreditCard(): ?bool
    {
        return $this->acceptsCreditCard;
    }

    public function setAcceptsCreditCard(?bool $acceptsCreditCard): static
    {
        $this->acceptsCreditCard = $acceptsCreditCard;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): static
    {
        $this->type = $type;

        return $this;
    }

    public function getBreakfastPrice(): ?int
    {
        return $this->breakfastPrice;
    }

    public function setBreakfastPrice(?int $breakfastPrice): static
    {
        $this->breakfastPrice = $breakfastPrice;

        return $this;
    }

    public function getSupperPrice(): ?int
    {
        return $this->supperPrice;
    }

    public function setSupperPrice(?int $supperPrice): static
    {
        $this->supperPrice = $supperPrice;

        return $this;
    }

    public function getOpeningDate(): ?\DateTimeInterface
    {
        return $this->openingDate;
    }

    public function setOpeningDate(?\DateTimeInterface $openingDate): static
    {
        $this->openingDate = $openingDate;

        return $this;
    }

    public function getClosingDate(): ?\DateTimeInterface
    {
        return $this->closingDate;
    }

    public function setClosingDate(?\DateTimeInterface $closingDate): static
    {
        $this->closingDate = $closingDate;

        return $this;
    }

    public function isIsMotorFriendly(): ?bool
    {
        return $this->isMotorFriendly;
    }

    public function setIsMotorFriendly(?bool $isMotorFriendly): static
    {
        $this->isMotorFriendly = $isMotorFriendly;

        return $this;
    }

    public function isIsReducedMobilityFriendly(): ?bool
    {
        return $this->isReducedMobilityFriendly;
    }

    public function setIsReducedMobilityFriendly(?bool $isReducedMobilityFriendly): static
    {
        $this->isReducedMobilityFriendly = $isReducedMobilityFriendly;

        return $this;
    }
}
