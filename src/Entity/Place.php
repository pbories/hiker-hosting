<?php

namespace App\Entity;

use App\Repository\PlaceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\UX\Turbo\Attribute\Broadcast;

#[ORM\Entity(repositoryClass: PlaceRepository::class)]
#[Broadcast]
class Place
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(nullable: true)]
    private ?float $latitude = null;

    #[ORM\Column(nullable: true)]
    private ?float $longitude = null;

    #[ORM\Column(nullable: true)]
    private ?int $distanceFromPath = null;

    #[ORM\Column(nullable: true)]
    private ?bool $hasBakery = null;

    #[ORM\Column(nullable: true)]
    private ?bool $hasGrocery = null;

    #[ORM\Column(nullable: true)]
    private ?bool $hasSuperMarket = null;

    #[ORM\Column(nullable: true)]
    private ?bool $hasRestaurant = null;

    #[ORM\Column(nullable: true)]
    private ?bool $hasPublicWater = null;

    #[ORM\Column(nullable: true)]
    private ?bool $hasDAB = null;

    #[ORM\OneToMany(mappedBy: 'place', targetEntity: Hosting::class)]
    private Collection $hostings;

    public function __construct()
    {
        $this->hostings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLatitude(?float $latitude): static
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(?float $longitude): static
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getDistanceFromPath(): ?int
    {
        return $this->distanceFromPath;
    }

    public function setDistanceFromPath(?int $distanceFromPath): static
    {
        $this->distanceFromPath = $distanceFromPath;

        return $this;
    }

    public function isHasBakery(): ?bool
    {
        return $this->hasBakery;
    }

    public function setHasBakery(?bool $hasBakery): static
    {
        $this->hasBakery = $hasBakery;

        return $this;
    }

    public function isHasGrocery(): ?bool
    {
        return $this->hasGrocery;
    }

    public function setHasGrocery(?bool $hasGrocery): static
    {
        $this->hasGrocery = $hasGrocery;

        return $this;
    }

    public function isHasSuperMarket(): ?bool
    {
        return $this->hasSuperMarket;
    }

    public function setHasSuperMarket(?bool $hasSuperMarket): static
    {
        $this->hasSuperMarket = $hasSuperMarket;

        return $this;
    }

    public function isHasRestaurant(): ?bool
    {
        return $this->hasRestaurant;
    }

    public function setHasRestaurant(?bool $hasRestaurant): static
    {
        $this->hasRestaurant = $hasRestaurant;

        return $this;
    }

    public function isHasPublicWater(): ?bool
    {
        return $this->hasPublicWater;
    }

    public function setHasPublicWater(?bool $hasPublicWater): static
    {
        $this->hasPublicWater = $hasPublicWater;

        return $this;
    }

    public function isHasDAB(): ?bool
    {
        return $this->hasDAB;
    }

    public function setHasDAB(?bool $hasDAB): static
    {
        $this->hasDAB = $hasDAB;

        return $this;
    }

    /**
     * @return Collection<int, Hosting>
     */
    public function getHostings(): Collection
    {
        return $this->hostings;
    }

    public function addHosting(Hosting $hosting): static
    {
        if (!$this->hostings->contains($hosting)) {
            $this->hostings->add($hosting);
            $hosting->setPlace($this);
        }

        return $this;
    }

    public function removeHosting(Hosting $hosting): static
    {
        if ($this->hostings->removeElement($hosting)) {
            // set the owning side to null (unless already changed)
            if ($hosting->getPlace() === $this) {
                $hosting->setPlace(null);
            }
        }

        return $this;
    }
}
