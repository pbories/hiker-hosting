<?php

namespace App\DataFixtures;

use App\Entity\Address;
use App\Entity\Bed;
use App\Entity\Hosting;
use App\Entity\PhoneNumber;
use App\Entity\Place;
use App\Entity\Room;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory as Faker;

/**
 * Class AppFixtures
 */
class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Faker::create('fr_ FR');

        $addresses = [];
        for ($i = 0; $i < 20; $i++) {
            $address = new Address();
            $address
                ->setFirstLine($faker->address)
                ->setZipCode($faker->postcode)
                ->setCity($faker->city);
            $manager->persist($address);
            $addresses[] = $address;
        }

        $places = [];
        for ($i = 0; $i < 10; $i++) {
            $place = new Place();
            $place
                ->setName($faker->city)
                ->setLatitude($faker->randomFloat())
                ->setLongitude($faker->randomFloat())
                ->setDistanceFromPath($faker->numberBetween([0, 2500]))
                ->setHasBakery($faker->randomElement([null, true, false]))
                ->setHasGrocery($faker->randomElement([null, true, false]))
                ->setHasSuperMarket($faker->randomElement([null, true, false]))
                ->setHasRestaurant($faker->randomElement([null, true, false]))
                ->setHasPublicWater($faker->randomElement([null, true, false]))
                ->setHasDAB($faker->randomElement([null, true, false]));
            $manager->persist($place);
            $places[] = $place;
        }

        $hostings = [];
        for ($i = 0; $i < 20; $i++) {
            $hosting = new Hosting();
            $hosting
                ->setName($faker->company)
                ->setPlace($faker->randomElement($places))
                ->setAddress($faker->passthrough($addresses[$i]))
                ->setEmail($faker->email)
                ->setMinCheckInTime(new DateTime($faker->time))
                ->setMaxCheckInTime(new DateTime($faker->time))
                ->setMaxCheckOutTime(new DateTime($faker->time))
                ->setHasKitchen($faker->randomElement([null, true, false]))
                ->setHasSupper($faker->randomElement([null, true, false]))
                ->setHasBreakFast($faker->randomElement([null, true, false]))
                ->setHasWashingMachine($faker->randomElement([null, true, false]))
                ->setHasWiFi($faker->randomElement([null, true, false]))
                ->setHasSheets($faker->randomElement([null, true, false]))
                ->setHasTowels($faker->randomElement([null, true, false]))
                ->setHasSwimmingPool($faker->randomElement([null, true, false]))
                ->setAcceptsCreditCard($faker->randomElement([true, false]))
                ->setType($faker->randomElement(['gîte', "chambre d'hôtes", 'hôtel', 'camping', 'accueil religieux']))
                ->setBreakfastPrice($faker->randomFloat(2, 0.0, 12.0))
                ->setSupperPrice($faker->randomFloat(2, 0.0, 25.0))
                ->setOpeningDate($faker->dateTime)
                ->setClosingDate($faker->dateTime)
                ->setIsMotorFriendly($faker->randomElement([null, true, false]))
                ->setIsReducedMobilityFriendly($faker->randomElement([null, true, false]));
            $manager->persist($hosting);
            $hostings[] = $hosting;
        }

        $rooms = [];
        for ($i = 0; $i < 60; $i++) {
            $room = new Room();
            $room
                ->setIsDormitory($faker->randomElement([true, false]))
                ->setHosting($faker->randomElement($hostings));
            $manager->persist($room);
            $rooms[] = $room;
        }

        $beds = [];
        for ($i = 0; $i < 120; $i++) {
            $bed = new Bed();
            $bed
                ->setIsDouble($faker->randomElement([true, false]))
                ->setPrice($faker->numberBetween([1200, 4500]))
                ->setRoom($faker->randomElement($rooms));
            $manager->persist($bed);
            $beds[] = $bed;
        }

        $phoneNumbers = [];
        for ($i = 0; $i < 40; $i++) {
            $phoneNumber = new PhoneNumber();
            $phoneNumber
                ->setIsMobile($faker->randomElement([true, false]))
                ->setFullNumber($faker->phoneNumber())
                ->setHosting($faker->randomElement($hostings));
            $manager->persist($phoneNumber);
            $phoneNumbers[] = $phoneNumber;
        }

        $manager->flush();
    }
}
